// Fill out your copyright notice in the Description page of Project Settings.


#include "Sleep_Projectile.h"

// Sets default values
ASleep_Projectile::ASleep_Projectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASleep_Projectile::BeginPlay()
{
	Super::BeginPlay();
	
	FTimerHandle L_Handle;
	GetWorld()->GetTimerManager().SetTimer(L_Handle, this, &ASleep_Projectile::DestroyFromTime, 5.f);
}

// Called every frame
void ASleep_Projectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASleep_Projectile::DestroyFromTime()
{
	Destroy();
}